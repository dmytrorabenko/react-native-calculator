import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  useWindowDimensions,
} from "react-native";
import calculator, {
  initialState as defaultInitialState,
} from "./src/calculator";
import Buttons from "./src/components/buttons";
import { getFontSize, formattedDisplay } from "./src/functions";
import { AppPropsType, CalculatorStateType } from "./types/types";
import { Audio } from "expo-av";
import AsyncStorage from "@react-native-async-storage/async-storage";

const STORAGE_KEY = "calculatorState";

const App: React.FC<AppPropsType> = () => {
  const { width, height } = useWindowDimensions();
  const isLandscape = width > height;
  const [calculatorState, setCalculatorState] =
    useState<CalculatorStateType>(defaultInitialState);
    
  const [displayValue, setDisplayValue] = useState<string>(
    calculatorState.onDisplay
  );
  const fontSize = getFontSize(isLandscape, displayValue.length);
  const sound = new Audio.Sound();
  let isSoundLoaded = false;
  let isLoading = false;

  const playSound = async () => {
    try {
      if (!isSoundLoaded && !isLoading) {
        isLoading = true;
        await sound.loadAsync(require("./assets/ios.mp3"));
        isSoundLoaded = true;
        isLoading = false;
      }
      if (isSoundLoaded) {
        await sound.playAsync();
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onPress = (button: string) => {
    playSound();
    const newState = calculator(calculatorState, button, isLandscape);
    setCalculatorState(newState);
  };

  useEffect(() => {
    const formattedDisplayValue = formattedDisplay(
      calculatorState.onDisplay,
      isLandscape
    );
    setDisplayValue(formattedDisplayValue);
  }, [width, height, calculatorState.onDisplay, isLandscape]);

 // Збереження стану в AsyncStorage при зміні стану
 useEffect(() => {
  AsyncStorage.setItem('calculatorState', JSON.stringify(calculatorState));
}, [calculatorState]);

// Відновлення стану при запуску додатка
useEffect(() => {
  const restoreState = async () => {
    try {
      const savedState = await AsyncStorage.getItem('calculatorState');
      if (savedState) {
        setCalculatorState(JSON.parse(savedState));
      }
    } catch (error) {
      console.error('Error restoring state', error);
    }
  };

  restoreState();
}, []);

useEffect(() => {
  return () => {
    sound.unloadAsync();
  };
}, []);




  useEffect(() => {
    return () => {
      sound.unloadAsync();
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={[
          styles.display,
          { fontSize },
          isLandscape && styles.landscapeDisplay,
        ]}
      >
        {displayValue}
      </Text>
      <Buttons
        onPress={onPress}
        isLandscape={isLandscape}
        calculatorState={calculatorState}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    justifyContent: "flex-end",
  },
  display: {
    textAlign: "right",
    paddingHorizontal: 20,
    marginBottom: 20,
    color: "#fff",
  },
  landscapeDisplay: {
    marginBottom: 5,
  },
});

export default App;
