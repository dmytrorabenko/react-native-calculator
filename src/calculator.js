export const initialState = {
  prevValue: "",
  secondValue: "",
  operator: null,
  onDisplay: "0",
  memory: null,
};

//Функціонал встановлення максимальної довжини значення
const getMaxValueLength = (value, isLandscape) => {
  const isDecimal = value.includes(".");
  return isLandscape ? (isDecimal ? 16 : 15) : isDecimal ? 10 : 9;
};

//Калькулятор
const calculate = (prevValue, secondValue, operator) => {
  const prevValueNum = parseFloat(prevValue);
  const secondValueNum = parseFloat(secondValue);
  switch (operator) {
    case "+":
      return prevValueNum + secondValueNum;
    case "-":
      return prevValueNum - secondValueNum;
    case "×":
      return prevValueNum * secondValueNum;
    case "÷":
      return secondValueNum !== 0 ? prevValueNum / secondValueNum : "Error";
  }
};

//  0123456789 - введення значень
const handleNumber = (prevState, button, isLandscape) => {
  const { prevValue, secondValue, operator, onDisplay } = prevState;
  const maxPrevValueLength = getMaxValueLength(prevValue, isLandscape);
  const maxSecondValueLength = getMaxValueLength(secondValue, isLandscape);

  if (
    secondValue === "" &&
    operator === null &&
    prevValue.length < maxPrevValueLength
  ) {
    if (onDisplay === "0") {
      return {
        ...prevState,
        onDisplay: button,
        prevValue: button,
      };
    } else if (onDisplay === "-0") {
      return {
        ...prevState,
        onDisplay: "-" + button,
        prevValue: "-" + button,
      };
    } else {
      return {
        ...prevState,
        onDisplay: onDisplay + button,
        prevValue: prevValue + button,
        secondValue: "",
        operator: null,
      };
    }
  } else if (
    prevValue !== "" &&
    operator !== null &&
    secondValue.length < maxSecondValueLength
  ) {
    return {
      ...prevState,
      onDisplay: secondValue + button,
      secondValue: secondValue + button,
    };
  }
  return prevState;
};

//  +-×÷ - операції над значеннями
const handleOperation = (prevState, button) => {
  const { prevValue, secondValue, operator, onDisplay } = prevState;

  if (prevValue !== "" && secondValue !== "" && operator !== null) {
    const result = String(calculate(prevValue, secondValue, operator));
    return {
      ...prevState,
      onDisplay: result,
      prevValue: result,
      secondValue: "",
      operator: button,
    };
  } else {
    return {
      ...prevState,
      onDisplay: onDisplay,
      prevValue: onDisplay,
      secondValue: secondValue,
      operator: button,
    };
  }
};
//  = - результат
const handleEqual = (prevState) => {
  const { prevValue, secondValue, operator } = prevState;

  if (prevValue !== "" && secondValue !== "") {
    const result = String(calculate(prevValue, secondValue, operator));
    return {
      ...prevState,
      onDisplay: result,
      prevValue: result,
      secondValue: "",
      operator: null,
    };
  }
  return prevState;
};
//  . - встановлення .
const handleDecimal = (prevState) => {
  const { operator, prevValue, secondValue, onDisplay } = prevState;

  if (!prevValue.includes(".") && prevValue.length < 9 && operator === null) {
    return {
      ...prevState,
      onDisplay: onDisplay + ".",
      prevValue: onDisplay + ".",
    };
  } else if (!secondValue.includes(".") && operator !== null) {
    if (secondValue === "") {
      return {
        ...prevState,
        onDisplay: "0.",
        secondValue: "0.",
      };
    } else {
      return {
        ...prevState,
        onDisplay: onDisplay + ".",
        secondValue: onDisplay + ".",
      };
    }
  }
  return prevState;
};
//  +/- -встановлення знаку -
const handleNegate = (prevState) => {
  const { onDisplay } = prevState;
  return {
    ...prevState,
    onDisplay:
      onDisplay.charAt(0) === "-" ? onDisplay.slice(1) : "-" + onDisplay,
  };
};
//  % - визначення відсотка від числа
const handlePercentage = (prevState) => {
  const { onDisplay } = prevState;
  const value = parseFloat(onDisplay) / 100;
  return {
    ...prevState,
    onDisplay: String(value),
  };
};

//  AC - очищення даних
const handleAllClear = (prevState) => ({
  ...initialState,
  memory: prevState.memory, // Зберігаємо значення пам'яті при очищенні інших полів
});

// Операція "mr" виведення значення з пам'яті на дисплей
const handleMemoryShow = (prevState) => {
  const { prevValue, secondValue, operator, memory } = prevState;
  if (memory !== null) {
    if (prevValue === "" && operator === null) {
      return {
        ...prevState,
        prevValue: memory,
        onDisplay: memory,
      };
    } else if (secondValue === "" && operator !== null) {
      return {
        ...prevState,
        secondValue: memory,
        onDisplay: memory,
      };
    }
  }
  return prevState;
};

//  m+ - додавання до результату в пам'яті
const handleMemoryPlus = (prevState) => {
  const { onDisplay, memory } = prevState;
  if (memory !== null) {
    const updatedMemory = parseFloat(memory) + parseFloat(onDisplay);
    return {
      ...prevState,
      memory: String(updatedMemory),
    }
  }else{
      return {
        ...prevState,
        memory: String(0 + parseFloat(onDisplay)),
      };
    }
};
//  m- - віднімання з результату в пам'яті
const handleMemoryMinus = (prevState) => {
  const { onDisplay, memory } = prevState;
  if (memory !== null) {
    const updatedMemory = parseFloat(memory) - parseFloat(onDisplay);
    return {
      ...prevState,
      memory: String(updatedMemory),
    };
  }
  else{
    return {
      ...prevState,
      memory: String(0 - parseFloat(onDisplay)),
    };
  }
};
//  mc - очищення результату в пам'яті
const handleMemoryClear = (prevState) => {
  const { memory } = prevState;
  if (memory !== null) {
    return {
      ...prevState,
      memory: null,
    };
  }
  return prevState;
};

const handleButtonPress = (prevState, button, isLandscape) => {
  switch (button) {
    case "+":
    case "-":
    case "×":
    case "÷":
      return handleOperation(prevState, button);
    case "=":
      return handleEqual(prevState);
    case ".":
      return handleDecimal(prevState);
    case "+/-":
      return handleNegate(prevState);
    case "%":
      return handlePercentage(prevState);
    case "AC":
      return handleAllClear(prevState);
    case "mc":
      return handleMemoryClear(prevState);
    case "m+":
      return handleMemoryPlus(prevState);
    case "m-":
      return handleMemoryMinus(prevState);
    case "mr":
      return handleMemoryShow(prevState);
    default:
      return handleNumber(prevState, button, isLandscape);
  }
};

const calculator = (prevState, button, isLandscape) => {
  return handleButtonPress(prevState, button, isLandscape);
};

export default calculator;

