//  Зменшення розміру шрифта при збілшенні кількості літер
const getFontSize = (isLandscape, length) => {
  if (!isLandscape) {
    if (length > 9) {
      return 50;
    } else if (length > 8) {
      return 55;
    } else if (length > 7) {
      return 60;
    } 
    else if (length > 6) {
      return 65;
    }
    else if (length > 5) {
      return 70;
    }else {
      return 80;
    }
  } else {
    return 50;
  }
};

//Розбиття масиву на частини
const chunkedButtons = (arr, chunkSize) => {
  return Array.from({ length: Math.ceil(arr.length / chunkSize) }, (v, i) =>
    arr.slice(i * chunkSize, i * chunkSize + chunkSize)
  );
};

// Функція експонтеціального форматування числа за потреби
const formattedDisplay = (value, isLandscape) => {
  let maxDisplayLength;
  if (isLandscape) {
    maxDisplayLength = value.includes(".") ? 15 : 16;
  } else {
    maxDisplayLength = value.includes(".") ? 10 : 9;
  }
  if (value.length > maxDisplayLength) {
    // Виводимо значення в експоненціальній формі з необхідною точністю
    return String(parseFloat(value).toExponential(2)); // -6, оскільки toExponential виводить "e+XX"
  } else {
    // Форматуємо число, додаючи пробіли кожні три символи
    return value.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }
};

export { getFontSize, chunkedButtons, formattedDisplay };
