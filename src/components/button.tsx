import React from "react";
import { TouchableOpacity, Text, StyleSheet, Dimensions } from "react-native";
import { ButtonPropsType } from "../../types/types";

const Button: React.FC<ButtonPropsType> = ({
  text,
  onPress,
  isLandscape,
  operator,
  memory,
}) => {
  const screen = Dimensions.get("screen");

  // розмір кнопок
  const getButtonSize = (text: string) => {
    const baseSize = isLandscape ? screen.width * 0.08 : screen.width * 0.22;
    const width = text === "0" ? baseSize * 2.15 : baseSize;
    const height = isLandscape ? baseSize * 0.75 : baseSize;
    return { width, height };
  };

  // колір фону кнопок
  const getButtonColor = (text: string) => {
    const buttonType1 = ["AC", "+/-", "%"];
    const buttonType2 = ["÷", "×", "+", "-", "="];
    const buttonType3 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."];

    if (buttonType1.includes(text)) {
      return { backgroundColor: "#D4D4D2" };
    } else if (buttonType2.includes(text) && operator === text) {
      return { backgroundColor: "#fff" };
    } else if (buttonType2.includes(text)) {
      return { backgroundColor: "#FF9500" };
    } else if (buttonType3.includes(text)) {
      return { backgroundColor: "#505050" };
    } else if( isLandscape && text === "mr" && memory !== null){
      return { backgroundColor: "#505050" };
    }else {
      return null;
    }
  };

  // колір тексту кнопки
  const getTextColor = (text: string) => {
    if (["AC", "+/-", "%"].includes(text)) {
      return { color: "#000" };
    } else if (operator === text) {
      return { color: "#FF9500" };
    } else {
      return { color: "#fff" };
    }
  };

  const buttonSize = getButtonSize(text);
  const buttonBgColor = getButtonColor(text);
  const textColor = getTextColor(text);

  return (
    <TouchableOpacity
      style={[styles.button, buttonSize, buttonBgColor]}
      onPress={() => onPress(text)}
    >
      <Text
        style={[textColor, isLandscape ? { fontSize: 20 } : { fontSize: 40 }]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    margin: 5,
    borderRadius: 50,
    backgroundColor: "#1C1C1C",
  },
});

export default Button;
