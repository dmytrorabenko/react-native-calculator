import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import Row from "./row";
import Button from "./button";
import { buttonsText } from "../buttonsText";
import { chunkedButtons } from "../functions";
import { ButtonsPropsType } from "../../types/types";

const Buttons: React.FC<ButtonsPropsType> = ({ isLandscape, onPress, calculatorState }) => {
  const [buttons, setButtons] = useState<string[]>(buttonsText.buttons);
  const [chunkSize, setChunkSize] = useState<number>(4);

  useEffect(() => {
    if (isLandscape) {
      setButtons(buttonsText.buttonsLandscape);
      setChunkSize(10);
    } else {
      setButtons(buttonsText.buttons);
      setChunkSize(4);
    }
  }, [isLandscape]);

  return (
    <View
      style={[
        styles.buttonsContainer,
        isLandscape && styles.landscapeButtonsContainer,
      ]}
    >
      {chunkedButtons(buttons, chunkSize).map((rowButtons, rowIndex) => (
        <Row key={rowIndex}>
          {rowButtons.map((item: string) => (
            <Button
              key={item}
              text={item}
              onPress={() => onPress(item)}
              isLandscape={isLandscape}
              operator={calculatorState.operator === item ? item : null}
              memory={calculatorState.memory === item ? item : null}
            />
          ))}
        </Row>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: "column",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingBottom: 30,
  },
  landscapeButtonsContainer: {
    paddingBottom: 5,
  },
});

export default Buttons;
