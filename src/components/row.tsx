import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import { RowPropsType } from "../../types/types";

const Row: React.FC<RowPropsType> = ({ children }) => {
  return <View style={styles.container}>{children}</View>;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
  } as ViewStyle, // Додав тип для ViewStyle
});

export default Row;
