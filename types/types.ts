import { ViewStyle } from "react-native";

export interface AppPropsType {}

export interface CalculatorStateType {
  prevValue: string,
  secondValue: string,
  operator: string | null,
  onDisplay: string,
  memory: string | null,
}

export interface ButtonsPropsType {
  isLandscape: boolean;
  onPress: (text: string) => void;
  calculatorState: {
    operator: string | null;
    memory: string | null;
  };
}

export interface RowPropsType {
  children: React.ReactNode;
}

export interface ButtonPropsType {
  text: string;
  onPress: (text: string) => void;
  isLandscape: boolean;
  operator?: string | null;
  memory?: string | null;
}

export interface StylesType {
  container: ViewStyle;
}
